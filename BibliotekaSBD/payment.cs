//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BibliotekaSBD
{
    using System;
    using System.Collections.Generic;
    
    public partial class payment
    {
        public int Id_payment { get; set; }
        public string type_of_payment { get; set; }
        public Nullable<decimal> amount_of_payment { get; set; }
        public int Id_order { get; set; }
    
        public virtual order order { get; set; }
    }
}
