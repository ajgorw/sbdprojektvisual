﻿using BibliotekaSBD.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaSBD
{
    class Program
    {
        static void Main(string[] args)
        {

            var context = new SbdBibliotekaEntities16();
            Logic.Methods met = new Methods();
            Logic.PublisherLogic pl = new PublisherLogic();
            OrderLogic orderLogic = new OrderLogic(context);
            //met.addLibrarian(new SbdBibliotekaEntities15());
            int choice = 0;
            Boolean state = true;
            while (true)
            {
                Console.WriteLine("1. Zaloguj sie jako uzytkownik" +
                 Environment.NewLine + "2. Zaloguj sie jako bibliotekarz" +
                 Environment.NewLine + "3. Zaloguj sie jako administrator" +
                 Environment.NewLine + "4. Wyjdz z programu :)");
                var ans = Console.ReadLine();
                if (int.TryParse(ans, out choice))
                {

                    switch (choice)
                    {
                        case 1:
                             Console.Clear();
                             Console.WriteLine("Wybierz uzytkownika wypisujac id: ");
                             met.showPeople();
                             
                             int userSelectedId = Convert.ToInt16(Console.ReadLine());

                             Console.Clear();

                             Console.WriteLine("1. Wypozycz ksiazke" +
                             Environment.NewLine + "2. Wyjdz do menu glownego");// +
                            // Environment.NewLine + "3. Wyjdz do menu glownego");
                            state = true;
                            while (state)
                            {
                                ans = Console.ReadLine();
                                if (int.TryParse(ans, out choice))
                                {
                                    switch (choice)
                                    {
                                        case 1:
                                            Console.Clear();
                                            met.showAvaliableBooks();
                                            Console.WriteLine("Wybierz książkę wypisujac jej tytuł: ");
                                            string userSelecteBook = Console.ReadLine();
                                            string isbnOfSelectedBook = orderLogic.getIsbnByTitleOfBook(userSelecteBook);
                                            orderLogic.createOrder(isbnOfSelectedBook, userSelectedId);
                                       
                                            Console.Clear();
                                            state = false;
                                            break;
                                        case 2:
                                            state = false;
                                            break;

                                        case 3:
                                            state = false;
                                            break;
                                        default:
                                            Console.WriteLine("Wrong selection, choose right option");
                                            break;

                                    }

                                }

                            }

                            break;
                        case 2:
                            state = true;
                            //met.showLibrarians();

                             Console.Clear();
                            Console.WriteLine("1. Dodaj ksiazke" +
                            Environment.NewLine + "2. Wyjdz do menu glownego");
                          
                            

                            while (state)
                            {
                                ans = Console.ReadLine();
                                if (int.TryParse(ans, out choice))
                                {
                                    switch (choice)
                                    {
                                        case 1:
                                            Console.Clear();
                                            met.addBook(context);
                                            state = false;
                                            break;
                                        case 2:
                                            state = false;
                                            break;

                                        default:
                                            Console.WriteLine("Wrong selection, choose right option");
                                            break;

                                    }

                                }

                            }

                            break;
                        case 3:
                            Console.Clear();
                            state = true;
                            Console.WriteLine("1. Dodaj bibliotekarza" +
                            Environment.NewLine + "2. Usuń bibliotekarza" +
                            Environment.NewLine + "3. Dodaj wydawce" +
                            Environment.NewLine + "4. Dodaj uzytkownik" +
                            Environment.NewLine + "5. Wyjdz do menu glownego");

                            while (state)
                            {
                                ans = Console.ReadLine();
                                if (int.TryParse(ans, out choice))
                                {
                                    switch (choice)
                                    {
                                        case 1:
                                            Console.Clear();
                                            met.addLibrarian(context);
                                            Console.Clear();
                                            state = false;
                                            break;
                                        case 2:
                                            met.removeLibrarian(context);
                                            state = false;
                                            break;

                                        case 3:
                                            pl.createPublisher(context);
                                            state = false;
                                            break;
                                        case 4:
                                            met.addUser(context);
                                            state = false;
                                            break;
                                        case 5:
                                            state = false;
                                            break;
                                        default:
                                            Console.WriteLine("Wrong selection, choose right option");
                                            break;

                                    }

                                }

                            }

                            break;
                        case 4:
                            return;
                        default:
                            Console.WriteLine("Wrong selection, choose right option");
                            break;
                    }
                }

                // AppDbContext appDbContext = new AppDbContext();
                /* var role = new role();
                 role.Id_role = 2;
                 role.name_of_role = "Administrator";

                 using (var context = new SbdBibliotekaEntities14())
                 {

                     context.role.Add(role);
                     context.SaveChanges();
                     Console.WriteLine("Dodano role");
                 }*/

                //var genre1 = new genre();
                //genre1.Id_genre = 1;
                //genre1.name_of_genre = "klasyka";

                // var genre2 = new genre();
                // genre2.Id_genre = 2;
                // genre2.name_of_genre = "horror";

                // var genre3 = new genre();
                // genre3.Id_genre = 3;
                // genre3.name_of_genre = "poezja";

                // var genre4 = new genre();
                // genre4.Id_genre = 4;
                // genre4.name_of_genre = "przygodowa";

                // var genre5 = new genre();
                // genre5.Id_genre = 5;
                // genre5.name_of_genre = "thirller";

                // var genre6 = new genre();
                // genre6.name_of_genre = "bajka";
                //   using(var context2 = new SbdBibliotekaEntities14())
                //   {
                //     context2.genre.Add(genre1);
                //   context.genre.Add(genre2);
                //   context.genre.Add(genre3);
                //   context.genre.Add(genre4);
                //   context.genre.Add(genre5);
                //  context.genre.Add(genre6);
                //     context2.SaveChanges();
                //} 
                //var role = new role();
                ////role.id_cover_type = 1;
                //role.name_of_role = "autor";

                // using (var context = new SbdBibliotekaEntities14())
                // {
                //    context.role.Add(role);
                //  context.SaveChanges();
                //}
                //var cov1 = new CoverType();
                // cov1.Name_Of_Type = "twarda";
                //var cov2 = new CoverType();
                // cov2.Name_Of_Type = "miekka";

                // using ()
                // {
                //context.CoverType.Add(cov1);
                //context.CoverType.Add(cov2);
                //    context.SaveChanges();
                // }
                //var context = new SbdBibliotekaEntities14();
                //Logic.MockAuthors mockAuthors = new Logic.MockAuthors();
                //mockAuthors.createFewAuthors();
                //var cov1 = new CoverType();
                //using (var context = new SbdBibliotekaEntities14())
                //{
                //    context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('CoverType', RESEED, 0)");
                //}
                /* MockAuthors mockAutors = new MockAuthors();
                 List<person> listOfAuthors =  mockAutors.getListOfAuthors(context);
                 foreach (var item in listOfAuthors)
                 {
                     Console.WriteLine(item.first_name);
                 }

                 Console.WriteLine("Id_author pierwszej osoby");
                 Console.WriteLine(mockAutors.getAuthorIdbyPersonId(context, listOfAuthors[0].Id_person));*/
                //AddressLogic logika = new AddressLogic();
                //logika.createAddressForPerson(context, 11);
                //PublisherLogic publisher = new PublisherLogic();
                //publisher.createPublisher(context);

                // Console.ReadKey();

                //var context = new SbdBibliotekaEntities14();

                // Logic.Methods met = new Methods();
                //met.addBook(context);


                //}

                /* using (var v = new SbdBibliotekaEntities14())
                 {
                     OrderLogic order = new OrderLogic(v);
                     order.createOrder("123456789111111", 7);
                 }*/
                /* SbdBibliotekaEntities15 v = new SbdBibliotekaEntities15();
                 role rola = new role();
                 rola.name_of_role = "Czytelnik";
                 v.role.Add(rola);
                 v.SaveChanges();*/
            }




        }
    }
}
    



