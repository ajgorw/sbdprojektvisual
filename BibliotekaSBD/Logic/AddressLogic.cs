﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaSBD.Logic
{
    class AddressLogic
    {
        //Metoda pobiera od admina dane do adresu dla Person
        //tworzy tabele address oraz joina person_address
        public void createAddressForPerson(SbdBibliotekaEntities16 ctx, int Id_person)
        {
            var context = ctx;

            //Tworze obiekt address
            address adres = new address();

            //Uzupelniam dane
            Console.WriteLine("Podaj ulice");
            adres.street = Console.ReadLine();

            Console.WriteLine("Podaj numer budynku");
            adres.number_of_the_building = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Podaj numer lokalu");
            adres.number_of_the_premises = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Podaj kod pocztowy");
            adres.post_number = Console.ReadLine();

            Console.WriteLine("Podaj miasto");
            adres.city = Console.ReadLine();

            Console.WriteLine("Podaj kraj");
            adres.country = Console.ReadLine();

            context.address.Add(adres);
            context.SaveChanges();

            //Moge odwolac sie do id nowo utworzonego obiektu po SaveChanges();
            int addressId = adres.Id_address;

            person_address joinTable = new person_address();
            joinTable.address_Id_address = addressId;
            joinTable.person_Id_person = Id_person;

            context.person_address.Add(joinTable);
            context.SaveChanges();
        }

        public void createAddressForPublisher(SbdBibliotekaEntities16 ctx, int Id_publisher)
        {
            var context = ctx;

            //Tworze obiekt address
            address adres = new address();

            //Uzupelniam dane
            Console.WriteLine("Podaj ulice");
            adres.street = Console.ReadLine();

            Console.WriteLine("Podaj numer budynku");
            adres.number_of_the_building = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Podaj numer lokalu");
            adres.number_of_the_premises = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Podaj kod pocztowy");
            adres.post_number = Console.ReadLine();

            Console.WriteLine("Podaj miasto");
            adres.city = Console.ReadLine();

            Console.WriteLine("Podaj kraj");
            adres.country = Console.ReadLine();

            context.address.Add(adres);
            context.SaveChanges();

            //Moge odwolac sie do id nowo utworzonego obiektu po SaveChanges();
            int addressId = adres.Id_address;

            publisher_address joinTable = new publisher_address();
            joinTable.address_Id_address = addressId;
            joinTable.publisher_Id_publisher = Id_publisher;

            context.publisher_address.Add(joinTable);
            context.SaveChanges();
        }

    }
}
