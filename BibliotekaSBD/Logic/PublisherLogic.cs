﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaSBD.Logic
{
    class PublisherLogic
    {
        public void createPublisher(SbdBibliotekaEntities16 ctx)
        {
            var context = ctx;
            publisher wydawaca = new publisher();

            //Uzupelniam dane
            Console.WriteLine("Podaj nazwe wydawcy");
            wydawaca.name_of_publisher = Console.ReadLine();


            context.publisher.Add(wydawaca);
            context.SaveChanges();

            Console.Clear();

            int idPublisher = wydawaca.Id_publisher;

            AddressLogic addressLogic = new AddressLogic();
            addressLogic.createAddressForPublisher(ctx, idPublisher);

        }


    }
}
