﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaSBD.Logic
{
    class Methods
    {
        public void addBook(SbdBibliotekaEntities16 ctx)
        {
            var book = new book();
            //string isbn;
            Console.WriteLine("Wpisz isbn ksiazki:");
            var next = Console.ReadLine();
            book.isbn = next;
            Console.WriteLine("Wpisz tytul ksiazki:");
            next = Console.ReadLine();
            book.title = next;
            Console.WriteLine("Wpisz podtytul ksiazki:");
            next = Console.ReadLine();
            book.subtitle = next;
            Console.WriteLine("Wpisz date wydania ksiazki:");
            book.release_date = DateTime.Parse(Console.ReadLine());
            book.state = "T";

            Console.Clear();
            book.id_cover_type = getCoverType();
            ctx.book.Add(book);
            ctx.SaveChanges();
            Console.Clear();

            addGenreForBook(ctx,book.isbn);
            Console.Clear();

            addPublisher(ctx,book.isbn);

            Console.Clear();

            MockAuthors mock = new MockAuthors();
            Console.Clear();

            List<person> listOfAuthors = mock.getListOfAuthors(ctx);
            Console.WriteLine("Wybierz autora podając jego id: ");
            foreach (var item in listOfAuthors)
            {
                Console.WriteLine(item.Id_person + " " + item.first_name + " " + item.surname);
            }
            int authorId = mock.getAuthorIdbyPersonId(ctx,Convert.ToInt16(Console.ReadLine()));

            author_book author_Book = new author_book();
            author_Book.book_isbn = book.isbn;
            author_Book.author_id_author = authorId;
            ctx.author_book.Add(author_Book);
            ctx.SaveChanges();
            Console.Clear();

            //var context = new SbdBibliotekaEntities14();
            //Logic.MockAuthors mockAuthors = new Logic.MockAuthors();
            //List<person> listOfAuthors = mockAuthors.getListOfAuthors(context);
            //foreach (var item in listOfAuthors)
            //{
            //    Console.WriteLine(item.first_name);

            //}
            //Console.ReadKey();

        }
        public void showPeople()
        {
            var ctx = new SbdBibliotekaEntities16();
            var people = ctx.person.ToList();
            foreach (var item in people)
            {
                
                {
                    Console.WriteLine(item.Id_person + "." + item.first_name + " " + item.surname);
                }


            }

        }
        public void showAvaliableBooks()
        {
            var ctx = new SbdBibliotekaEntities16();
            var books = ctx.book.Where(a => a.state == "T").ToList();
            foreach (var item in books)
            {

                {
                    Console.WriteLine(item.title + " " + item.isbn);
                }


            }

        }
        public void addGenreForBook(SbdBibliotekaEntities16 ctx, String isbn)
        {
            var bo = new book_genre();
            var genre = getSpecifedGenre(ctx);

            bo.book_isbn = isbn;
            bo.genre_id_genre = genre; 
            
                ctx.book_genre.Add(bo);
                ctx.SaveChanges();
            
        }
        public int getSpecifedGenre(SbdBibliotekaEntities16 context)
        {
            var ctx = context;
            var genres = ctx.genre.ToList();
            var id = 0;
            Console.WriteLine("Wpisz nazwe gatunku:");
            var next = Console.ReadLine();
            foreach (var item in genres)
            {
                if (item.name_of_genre == next)
                {
                    id = item.Id_genre;
                }


            }
            return id;
        }

        public int getSpecifedPublisher(SbdBibliotekaEntities16 context)
        {
            var ctx = context;
            var publisher = ctx.publisher.ToList();
            var id = 1;
            Console.WriteLine("Wpisz wydawnictwo:");
            var next = Console.ReadLine();
            foreach (var item in publisher)
            {
                if (item.name_of_publisher == next)
                {
                    id = item.Id_publisher;
                }

            }
            return id;
        }
        public void addPublisher(SbdBibliotekaEntities16 ctx, String isbn)
        {
            var bp = new book_publisher();
            var publish = getSpecifedPublisher(ctx);


            bp.isbn_id_isbn = isbn;
            bp.publisher_id_publisher = publish;

            ctx.book_publisher.Add(bp);
            ctx.SaveChanges();
        }
        public void addLibrarian(SbdBibliotekaEntities16 context)
        {

            var person = new person();
            Console.WriteLine("Wpisz pesel bibliotekarza:");
            var next = Console.ReadLine();
            person.pesel = next;
            Console.WriteLine("Wpisz imie bibliotekarza:");
            next = Console.ReadLine();
            person.first_name = next;
            Console.WriteLine("Wpisz nazwisko bibliotekarza:");
            next = Console.ReadLine();
            person.surname = next;
            Console.WriteLine("Wpisz date urodzenia:");
            next = Console.ReadLine();
            person.date_of_birth = Convert.ToDateTime(next);
            Console.WriteLine("Wpisz numer telefonu:");
            next = Console.ReadLine();
            person.phone = next;
            Console.WriteLine("Wpisz e-mail");
            next = Console.ReadLine();
            person.email = next;

            context.person.Add(person);
            context.SaveChanges();

            person_role Rola = new person_role();

            Rola.role_Id_role = context.role.SingleOrDefault(c => c.name_of_role == "Bibliotekarz").Id_role;
            Rola.person_Id_person = person.Id_person;

            context.person_role.Add(Rola);
            context.SaveChanges();



        }
        public void addUser(SbdBibliotekaEntities16 context)
        {
            var person = new person();
            Console.WriteLine("Wpisz pesel uzytkownika:");
            var next = Console.ReadLine();
            person.pesel = next;
            Console.WriteLine("Wpisz imie uzytkownika:");
            next = Console.ReadLine();
            person.first_name = next;
            Console.WriteLine("Wpisz nazwisko uzytkownika:");
            next = Console.ReadLine();
            person.surname = next;
            Console.WriteLine("Wpisz date urodzenia:");
            next = Console.ReadLine();
            person.date_of_birth = Convert.ToDateTime(next);
            Console.WriteLine("Wpisz numer telefonu:");
            next = Console.ReadLine();
            person.phone = next;
            Console.WriteLine("Wpisz e-mail");
            next = Console.ReadLine();
            person.email = next;

            context.person.Add(person);
            context.SaveChanges();

            person_role Rola = new person_role();

            Rola.role_Id_role = context.role.SingleOrDefault(c => c.name_of_role == "Uzytkownik").Id_role;
            Rola.person_Id_person = person.Id_person;

            context.person_role.Add(Rola);
            context.SaveChanges();
        }
        public void showAllLibrarians(SbdBibliotekaEntities16 context )
        {
            //getting id role of librarian
            int libId = context.role.First(a => a.name_of_role == "Bibliotekarz").Id_role;
            //getting all person_role where id_role == id_librarian
            var list = context.person.ToList();

        

        }
        public int getCoverType()
        {
            var ctx = new SbdBibliotekaEntities16();
            var types = ctx.CoverType.ToList();
            var id = 0;
            Console.WriteLine("Wpisz rodzaj okladki:");
            var next = Console.ReadLine();
            foreach (var item in types)
            {
                if (item.Name_Of_Type == next)
                {
                    id = item.id_cover_type;
                }

            }
            return id;

        }
        public void removeLibrarian(SbdBibliotekaEntities16 ctx)
        {
            
            Console.WriteLine("Wpisz pesel bibliotekarza do usuniecia z bazy:");
            var pesel = Console.ReadLine();
         

            person toDelate = ctx.person.First(a => a.pesel == pesel);
            Console.WriteLine("Czy jestes pewien? (tak/nie)");
            var next = Console.ReadLine();
            if (next == "tak")
            {
                    ctx.person.Attach(toDelate);
                    ctx.person.Remove(toDelate);
                    ctx.SaveChanges();
                
            }
            else
            {
                return;
            }
        }
    }
}
