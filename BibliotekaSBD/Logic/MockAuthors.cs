﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaSBD.Logic
{
    class MockAuthors
    {
        //Tworzy tabele person, role i author

        public void createAuthor(SbdBibliotekaEntities16 ctx, string firstName, string surname, DateTime bornTime,
                               DateTime deathTime)
        {
            var context = ctx;
            //Tworze wpis do tabeli person
            person firstPerson = new person();
            firstPerson.date_of_birth = bornTime;
            firstPerson.date_of_death_ = deathTime;
            firstPerson.first_name = firstName;
            firstPerson.surname = surname;
            context.person.Add(firstPerson);
            context.SaveChanges();

            //Tworze obiekt joina laczacego person i role. Role autora jest stworzona.
            person_role pierwszaRola = new person_role();
            pierwszaRola.role_Id_role = context.role.SingleOrDefault(c => c.name_of_role == "Autor").Id_role;

            pierwszaRola.person_Id_person = context.person.SingleOrDefault(c => c.first_name == firstName
                                                                           && c.surname == surname).Id_person;

            context.person_role.Add(pierwszaRola);
            context.SaveChanges();

            author auth = new author();
            auth.Id_person = pierwszaRola.person_Id_person;

            context.author.Add(auth);
            context.SaveChanges();
        }
        public void createFewAuthors()
        {
            var context = new SbdBibliotekaEntities16();
            createAuthor(context, "John", "Johnos", new DateTime(1921, 1, 6), new DateTime(2000, 1, 1));
            createAuthor(context, "Joseph", "Conrad", new DateTime(1857, 12, 3), new DateTime(1924, 1, 3));
            createAuthor(context, "Ernest", "Hemingway", new DateTime(1899, 7, 21), new DateTime(1961, 7, 2));

        }
        public List<person> getListOfAuthors(SbdBibliotekaEntities16 ctx)
        {
            var context = ctx;

            var authors = ctx.author.ToList();
           
            var persons = new List<person>();

            for (int i = 0; i < authors.Count; i++)
            {
                person p = ctx.person.Find(authors[i].Id_person);
                persons.Add(p);
            }
            return persons;
         
        }
        //Metoda zwraca id autora majac dostep do polaczenia i id_osoby
        public int getAuthorIdbyPersonId(SbdBibliotekaEntities16 ctx, int personId)
        {
            var context = ctx;
            int id = context.author.Single(a => a.Id_person == personId).Id_author;

            return id;
        }
    }
}
