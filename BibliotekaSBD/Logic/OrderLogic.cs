﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaSBD.Logic
{
    class OrderLogic
    {
        private SbdBibliotekaEntities16 context;

        public OrderLogic(SbdBibliotekaEntities16 ctx)
        {
            context = ctx;
        }

        public void createOrder(String isbn, int personId)
        {
            
            //Tworze obiekt order
            order order = new order();
            order.date_of_rental = DateTime.Now.Date;
            order.date_of_return = DateTime.Now.Date.AddDays(14);

            context.order.Add(order);
            context.SaveChanges();

            //Pobieram id Orderu
            int idOrder = order.Id_order;

          

            //Tworze tabele book_order
            book_order bookOrder = new book_order();
            bookOrder.book_isbn = isbn;
            bookOrder.order_id_order = idOrder;


            context.book_order.Add(bookOrder);
            context.SaveChanges();

            var result = context.order.First(b => b.Id_order == idOrder);
            if (result != null)
            {
                result.book_order.Add(bookOrder);
                 context.SaveChanges();
            }


            createOrder_PersonAndMerge(personId, idOrder);

            addPaymentToOrder(idOrder);

        }

        public void createOrder_PersonAndMerge(int personId, int orderId)
        {
            order_person order_Person = new order_person();
            order_Person.person_Id_person = personId;
            order_Person.order_Id_order = orderId;
            context.order_person.Add(order_Person);
            context.SaveChanges();

        }
        public string getIsbnByTitleOfBook(string name)
        {

            return
                context.book.SingleOrDefault(c => c.title == name).isbn;

        }
        public int addPaymentToOrder(int orderId)
        {
            payment payment = new payment();
            Console.WriteLine("Jeżeli chcesz przekazac darowizne na rzecz biblioteki wpisz kwote\nJezeli nie- wpisz 0");
            payment.amount_of_payment = Convert.ToInt16(Console.ReadLine());
            payment.Id_order = orderId;
            context.payment.Add(payment);
            context.SaveChanges();

            return payment.Id_payment;
        }

    }
}
